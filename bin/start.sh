#!/bin/bash

homepath=$(cd `dirname $0`/..; pwd)
cd $homepath
nohup python $homepath/datasyncer/sync/sync_all.py $homepath 1>${homepath}/log/datasyncer.log 2>&1 2>${homepath}/log/error.log &
tailf ${homepath}/log/datasyncer.log
