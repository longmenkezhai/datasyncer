# -*- coding: UTF-8 -*-
import sys
import threading
import time
import traceback

# sys.path.append(sys.argv[1])
sys.path.append('/home/git/datasync')
import conf.config_handler as ch
from datasyncer.utils.parse_util import ParseHandler
from datasyncer.utils.postgre_util import PgClient as Pc
from datasyncer.utils.kafka_util import KafkaUtil as Ku
import datasyncer.utils.datasyncer_util as du

max_commit = ch.max_commit
commit_interval = ch.commit_interval
pg_client = Pc(database=ch.gp_db, user=ch.gp_user, password=ch.gp_pwd, host=ch.gp_host)


class SyncExecuter(object):
    def __init__(self, executer_pool):
        self.ku = Ku()
        self.executer_pool = executer_pool
        self.kafka_table = 'bi_kafka_offset'

    def insert_kafka_status(self, offset):
        _t = du.get_table(self.kafka_table)
        _t.insert().values(topic=self.ku.DEFAULT_TOPIC, offset=offset).execute()

    def update_kafka_status(self, offset):
        _t = du.get_table(self.kafka_table)
        _t.update().where(_t.c.topic == self.ku.DEFAULT_TOPIC).values(offset=offset).execute()

    def get_kafka_status(self, topic):
        _t = du.get_table(self.kafka_table)
        res = _t.select().where(_t.c.topic == topic).execute()
        res = res.fetchone()
        if res:
            offset = res[1]
            return offset
        else:
            return None

    def run(self):
        consumer = self.ku.get_consumer()
        consumer.unsubscribe()
        consumer.assign([self.ku.DEFAULT_TOPIC_PARTITION])
        current_offset = self.get_kafka_status(self.ku.DEFAULT_TOPIC)
        print 'current_offset: %s' % current_offset
        if current_offset is None:
            self.insert_kafka_status(-2)
            offset = -2
        else:
            offset = current_offset
        if offset == -2:
            consumer.seek_to_beginning(self.ku.DEFAULT_TOPIC_PARTITION)
        elif offset == -1:
            consumer.seek_to_end(self.ku.DEFAULT_TOPIC_PARTITION)
        else:
            consumer.seek(self.ku.DEFAULT_TOPIC_PARTITION, offset=int(offset))
        wait_time = ch.wait_time
        print 'datasync start ...'
        for message in consumer:
            binlog = message.value
            self.update_kafka_status(message.offset)
            # print 'current id is {0}'.format(n)
            if ParseHandler.is_ddl(binlog):
                # ParseHandler.execute_ddl_sql(binlog, pg_client)
                continue
            while not self.executer_pool.append(binlog):
                print 'execute pool is full , current id is {0} , wait for {1}s'.format(message.offset, wait_time)
                time.sleep(wait_time)


class ExecuterPool(object):
    def __init__(self):
        self.pg_client = pg_client
        self.pool = []
        self.max_commit = max_commit
        self.lock = False

    def clear(self):
        self.pool = []

    def is_full(self):
        return len(self.pool) == self.max_commit

    def append(self, elem):
        if len(self.pool) < self.max_commit and not self.lock:
            self.pool.append(elem)
            return 1
        else:
            return 0

    def commit(self):
        try:
            if not self.pool:
                # print 'the execute pool is empty'
                return 1
            self.lock = True
            ph = ParseHandler(self.pool)
            print 'start to execute the parse binlog'
            _start = time.time()
            ph.parse_binlog()
            _end = time.time()
            print 'execute the parse binlog , use {0}s'.format(_end - _start)
            print 'start to execute the delete sql'
            _start = time.time()
            ph.do_delete(self.pg_client)
            _end = time.time()
            print 'execute the delete sql , use {0}s'.format(_end - _start)
            print 'start to execute the copy sql'
            _start = time.time()
            ph.do_copy(self.pg_client)
            _end = time.time()
            print 'execute the copy sql , use {0}s'.format(_end - _start)
            print 'clear the execute pool'
        except SnycRunTimeException:
            msg = traceback.format_exc()
            print msg
        finally:
            self.clear()
            self.lock = False


class SnycRunTimeException(RuntimeError):
    def __init__(self, error_msg='commit error,please check the greenplum db'):
        RuntimeError.__init__(self, error_msg=error_msg)


def run():
    ep = ExecuterPool()
    se = SyncExecuter(ep)
    t1 = threading.Thread(target=se.run, args=())
    t1.start()
    start = time.time()
    while 1:
        try:
            if ep.is_full() or time.time() - start >= commit_interval:
                ep.commit()
                start = time.time()
            else:
                time.sleep(1)
        except SnycRunTimeException:
            msg = traceback.format_exc()
            print msg
