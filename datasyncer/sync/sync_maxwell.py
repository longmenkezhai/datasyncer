# -*- coding: UTF-8 -*-

"""
Help for Maxwell:

Option                              Description
------                              -----------
--config                            location of config file
--log_level                         log level, one of DEBUG|INFO|WARN|ERROR

--host                              mysql host with write access to maxwell database
--port                              port for host
--user                              username for host
--password                          password for host
--jdbc_options                      additional jdbc connection options
--binlog_connector                  run with new binlog connector library

--replication_host                  mysql host to replicate from (if using separate schema and replication servers)
--replication_user                  username for replication_host
--replication_password              password for replication_host
--replication_port                  port for replication_host
--schema_host                       overrides replication_host for retrieving schema
--schema_user                       username for schema_host
--schema_password                   password for schema_host
--schema_port                       port for schema_host

--producer                          producer type: stdout|file|kafka|kinesis
--output_file                       output file for 'file' producer
--producer_partition_by             database|table|primary_key|column, kafka/kinesis producers will partition by this value
--producer_partition_columns        with producer_partition_by=column, partition by the value of these columns.  comma separated.
--producer_partition_by_fallback    database|table|primary_key, fallback to this value when when sing 'column' partitioning and the
                                      columns are not present in the row
--kafka.bootstrap.servers           at least one kafka server, formatted as HOST:PORT[,HOST:PORT]
--kafka_partition_hash              default|murmur3, hash function for partitioning
--kafka_topic                       optionally provide a topic name to push to. default: maxwell
--kafka_key_format                  how to format the kafka key; array|hash
--kafka_version                     use kafka 0.8, 0.9, 0.10, 0.10.1, or 0.10.2 producer (default 0.9)
--kinesis_stream                    kinesis stream name

--output_binlog_position            produced records include binlog position; [true|false]. default: false
--output_gtid_position              produced records include gtid position; [true|false]. default: false
--output_commit_info                produced records include commit and xid; [true|false]. default: true
--output_nulls                      produced records include fields with NULL values [true|false]. default: true
--output_server_id                  produced records include server_id; [true|false]. default: false
--output_thread_id                  produced records include thread_id; [true|false]. default: false
--output_ddl                        produce DDL records to ddl_kafka_topic [true|false]. default: false
--ddl_kafka_topic                   optionally provide an alternate topic to push DDL records to. default: kafka_topic

--bootstrapper                      bootstrapper type: async|sync|none. default: async

--replica_server_id                 server_id that maxwell reports to the master.  See docs for full explanation.
--client_id                         unique identifier for this maxwell replicator
--schema_database                   database name for maxwell state (schema and binlog position)
--max_schemas                       deprecated.
--init_position                     initial binlog position, given as BINLOG_FILE:POSITION:HEARTBEAT
--replay                            replay mode, don't store any information to the server
--master_recovery                   (experimental) enable master position recovery code
--gtid_mode                         (experimental) enable gtid mode
--ignore_producer_error             Maxwell will be terminated on kafka/kinesis errors when false. Otherwise, those producer errors
                                      are only logged. Default to true

--include_dbs                       include these databases, formatted as include_dbs=db1,db2
--exclude_dbs                       exclude these databases, formatted as exclude_dbs=db1,db2
--include_tables                    include these tables, formatted as include_tables=db1,db2
--exclude_tables                    exclude these tables, formatted as exclude_tables=tb1,tb2
--exclude_columns                   exclude these columns, formatted as exclude_columns=col1,col2
--blacklist_dbs                     ignore data AND schema changes to these databases, formatted as blacklist_dbs=db1,db2. See the
                                      docs for details before setting this!
--blacklist_tables                  ignore data AND schema changes to these tables, formatted as blacklist_tables=tb1,tb2. See the
                                      docs for details before setting this!

--metrics_prefix                    the prefix maxwell will apply to all metrics
--metrics_type                      how maxwell metrics will be reported, at least one of slf4j|jmx|http|datadog
--metrics_slf4j_interval            the frequency metrics are emitted to the log, in seconds, when slf4j reporting is configured
--metrics_http_port                 the port the server will bind to when http reporting is configured
--metrics_datadog_type              when metrics_type includes datadog this is the way metrics will be reported, one of udp|http
--metrics_datadog_tags              datadog tags that should be supplied, e.g. tag1:value1,tag2:value2
--metrics_datadog_interval          the frequency metrics are pushed to datadog, in seconds
--metrics_datadog_apikey            the datadog api key to use when metrics_datadog_type = http
--metrics_datadog_host              the host to publish metrics to when metrics_datadog_type = udp
--metrics_datadog_port              the port to publish metrics to when metrics_datadog_type = udp

--help                              display help
"""
import os
import conf.config_handler as ch
import datasyncer.utils.datasyncer_util as du


def run_maxwell():
    def get_include():
        sync_table = du.get_table("bi_sync_table")
        res = sync_table.select().execute()
        d = [(row[1], row[3]) for row in res]
        tables = ','.join(list(set([e[0] for e in d])))
        dbs = ','.join(list(set([e[1] for e in d])))
        return {'dbs': dbs, 'tables': tables}

    maxwell_bin = '{0}/lib/maxwell-1.10.4/bin/maxwell'.format(ch.HOME_PATH)
    maxwell_db_opt = "--user={0} --password={1} --host={2}".format(ch.mw_user, ch.mw_pwd, ch.mw_host)
    maxwell_replication_db_opt = "--replication_host={0} --replication_user={1} --replication_password={2}".format(
        ch.rp_host, ch.rp_user, ch.rp_pwd)
    kafka_opt = "--producer=kafka --kafka.bootstrap.servers={0} --kafka_topic={1}".format(ch.kafka_hosts_str,
                                                                                          ch.kafka_topic)
    ddl_opt = '--output_ddl={0}'.format(ch.ddl)
    includes = get_include()
    include_opt = "--include_dbs={0} --include_tables={1}".format(includes['dbs'], includes['tables'])
    # log_path = ch.HOME_PATH + '/log/maxwell.log'
    maxwell_start = """
    {maxwell_bin} {maxwell_db_opt} {maxwell_replication_db_opt} {kafka_opt} {include_opt} {ddl_opt}
    """.format(maxwell_bin=maxwell_bin, maxwell_db_opt=maxwell_db_opt,
               maxwell_replication_db_opt=maxwell_replication_db_opt, kafka_opt=kafka_opt, include_opt=include_opt,
               ddl_opt=ddl_opt)
    # maxwell_start_nohup = """
    # nohup {maxwell_start} > {log_path} &2 > 1
    # """.format(maxwell_start=maxwell_start, log_path=log_path)
    print maxwell_start
    os.system(maxwell_start)

# run_maxwell()
