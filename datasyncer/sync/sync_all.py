# -*- coding: UTF-8 -*-

import sys
import os

sys.path.append(sys.argv[1])
import sync_executer_batch as seb
import sync_maxwell as sm
import conf.config_handler as ch
from sync_table_handler import SyncTableHandler as sth
import threading
import time

print 'pid : ' + str(os.getpid())
print 'ppid : ' + str(os.getppid())
pid_file = open('%s/bin/.pid' % ch.HOME_PATH, 'w')
pid_file.write(str(os.getpid()))
pid_file.flush()
pid_file.close()
maxwell_thread = threading.Thread(target=sm.run_maxwell, args=())
maxwell_thread.start()
time.sleep(10)
if maxwell_thread.isAlive():
    sync_table_handler = sth()
    sync_table_handler.do_sync_table_data()
    seb.run()
else:
    sys.exit(1)
