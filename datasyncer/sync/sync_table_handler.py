# -*- coding: UTF-8 -*-
import os
from datasyncer.utils import sync_table_util as stu
import conf.config_handler as ch
from sqlalchemy import *
import datasyncer.utils.write_datax_conf as wdc
import time
import datasyncer.utils.datasyncer_util as cu

"""
sync = 0 table not sync
sync = 1 table sync
sync = -1 drop the table
"""
NOT_SYNC = 0
SYNC = 1
DROPPED = -1


class SyncTableHandler(object):
    def __init__(self):
        self.sync_table = cu.get_table('bi_sync_table')

    def add_sync_table(self, source_table, source_db=None, target_table=None, target_db=None, sub_table=0):
        if not source_db:
            source_db = ch.mysql_db
        if not target_db:
            target_db = source_db
        if not target_table:
            target_table = source_table
        self.sync_table.insert().values(source_table=source_table, target_table=target_table, source_db=source_db,
                                        target_db=target_db,
                                        create_time=time.time(), update_time=time.time(), sub_table=sub_table,
                                        sync=NOT_SYNC).execute()

    def _update_sync_table(self, source_table, sync):
        self.sync_table.update().where(self.sync_table.c.source_table == source_table).values(update_time=time.time(),
                                                                                              sync=sync).execute()

    def do_drop_target_table(self):
        result = select([self.sync_table.c.source_table]).where(self.sync_table.c.sync == DROPPED).execute()
        for row in result:
            _table = row[0]
            print 'begin to drop table {0}'.format(_table)
            stu.drop_table(_table)

    @staticmethod
    def __sync_single_table(mysql_db, mysql_table, gp_db, gp_table):
        path = ch.HOME_PATH + '/tasks/%s.json' % gp_table
        log_path = ch.HOME_PATH + '/log/datasync_{0}.log'.format(gp_table)
        datax = ch.HOME_PATH + '/lib/datax/bin/datax.py'
        conf = wdc.get_conf(mysql_db=mysql_db, mysql_table=mysql_table, gp_db=gp_db, gp_table=gp_table)
        wdc.write_file(path=path, conf=conf)
        # cmd = 'nohup python {0} {1} > {2} &2 > 1'.format(datax, path, log_path)
        cmd = 'python {0} {1}'.format(datax, path)
        print cmd
        os.system(cmd)

    def do_sync_table_data(self):
        result = self.sync_table.select().where(self.sync_table.c.sync == NOT_SYNC).execute()
        for row in result:
            mysql_table, gp_table, mysql_db, gp_db = row[1], row[2], row[3], row[4]
            print 'begin to synchronize data {0}'.format(mysql_table)
            stu.sync_table(mysql_db, mysql_table)
            self.__sync_single_table(mysql_db, mysql_table, gp_db, gp_table)
            self._update_sync_table(mysql_table, SYNC)

    def do_sync_single_table_data(self, mysql_table, gp_table, mysql_db, gp_db):
        stu.sync_table(mysql_db, mysql_table)
        self.__sync_single_table(mysql_db, mysql_table, gp_db, gp_table)
