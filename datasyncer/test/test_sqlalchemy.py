# -*- coding: UTF-8 -*-

from sqlalchemy.orm import *
from sqlalchemy import *
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.dialects.mysql import DOUBLE, TINYINT


@compiles(DOUBLE, 'postgresql')
def compile_double_postgres_mysql(element, compiler, **kw):
    return compiler.visit_REAL(element, **kw)


@compiles(TINYINT, 'postgresql')
def compile_tinyint_postgres_mysql(element, compiler, **kw):
    return compiler.visit_SMALLINT(element, **kw)


MYSQL_CONNECT_STRING = 'mysql://ingage:ingage@192.168.0.75/isales_db_isales_001?charset=utf8'
PG_CONNECT_STRING = 'postgres://gpadmin:gpadmin@192.168.0.196/test'

source_engine = create_engine(MYSQL_CONNECT_STRING, echo=True)
target_engine = create_engine(PG_CONNECT_STRING, echo=True)
source_meta = MetaData(source_engine)
target_meta = MetaData(target_engine)

# DB_Session = sessionmaker(source_engine)
# session = DB_Session()
# tables = session.execute('show tables').fetchall()
# for table in tables:
#     try:
#         t = table[0]
#         source_table = Table(t, source_meta, autoload=True)
#         target_table = source_table.tometadata(target_meta, schema=None)
#         target_table.create(target_engine, checkfirst=True)
#     except Exception:
#         print 'a'

from sqlalchemy import event


def listen_for_reflect(inspector, table, column_info):
    "listen for the 'column_reflect' event"
    column_info['name'] = column_info['name'].lower()


source_table = Table('a_account', source_meta, autoload=True, listeners=[
    ('column_reflect', listen_for_reflect)
])
target_table = source_table.tometadata(target_meta, schema=None)
target_table.indexes.clear()
target_table.create(target_engine, checkfirst=True)
