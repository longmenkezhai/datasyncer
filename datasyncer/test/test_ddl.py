# -*- coding: UTF-8 -*-
from datasyncer.utils.parse_util import ParseHandler as PH
from sqlalchemy import Table, MetaData, String, Column
from migrate.changeset.schema import create_column

PG_CONNECT_STRING = 'postgres://gpadmin:gpadmin@192.168.0.196/test'

meta = MetaData(bind=PG_CONNECT_STRING)
account = Table('test', meta, autoload=True)
name = Column('name', String(128))
create_column(name, account)

ddl1 = {u'old': {u'table': u'aa', u'charset': u'latin1', u'primary-key': [],
                 u'columns': [{u'type': u'int', u'name': u'id', u'signed': True},
                              {u'charset': u'latin1', u'type': u'varchar', u'name': u'name'}], u'database': u'test'},
        u'database': u'test', u'ts': 1503024779000, u'sql': u'alter table aa add column (name2 varchar(255))',
        u'table': u'aa', u'type': u'table-alter', u'def': {u'table': u'aa', u'charset': u'latin1', u'primary-key': [],
                                                           u'columns': [
                                                               {u'type': u'int', u'name': u'id', u'signed': True},
                                                               {u'charset': u'latin1', u'type': u'varchar',
                                                                u'name': u'name'},
                                                               {u'charset': u'latin1', u'type': u'varchar',
                                                                u'name': u'name2'}], u'database': u'test'}}

ddl2 = {u'old': {u'table': u'aa', u'charset': u'latin1', u'primary-key': [],
                 u'columns': [{u'type': u'int', u'name': u'id', u'signed': True},
                              {u'charset': u'latin1', u'type': u'varchar', u'name': u'name'},
                              {u'charset': u'latin1', u'type': u'varchar', u'name': u'name2'},
                              {u'charset': u'latin1', u'type': u'varchar', u'name': u'name3'},
                              {u'charset': u'latin1', u'type': u'varchar', u'name': u'name4'}], u'database': u'test'},
        u'database': u'test', u'ts': 1503026051000, u'sql': u'alter table aa add column (name1 varchar(255))',
        u'table': u'aa', u'type': u'table-alter', u'def': {u'table': u'aa', u'charset': u'latin1', u'primary-key': [],
                                                           u'columns': [
                                                               {u'type': u'int', u'name': u'id', u'signed': True},
                                                               {u'charset': u'latin1', u'type': u'varchar',
                                                                u'name': u'name'},
                                                               {u'charset': u'latin1', u'type': u'varchar',
                                                                u'name': u'name2'},
                                                               {u'charset': u'latin1', u'type': u'varchar',
                                                                u'name': u'name3'},
                                                               {u'charset': u'latin1', u'type': u'varchar',
                                                                u'name': u'name4'},
                                                               {u'charset': u'latin1', u'type': u'varchar',
                                                                u'name': u'name1'}], u'database': u'test'}}
