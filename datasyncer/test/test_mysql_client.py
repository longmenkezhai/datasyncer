# -*- coding: UTF-8 -*-

import unittest

import conf.config_handler as ch
from datasyncer.utils.mysql_util import Connection


class TestPgClint(unittest.TestCase):
    def setUp(self):
        self.mysql_client = Connection(mysql_db=ch.mysql_db, user=ch.mysql_user, password=ch.mysql_pwd, mysql_host=ch.mysql_host)

    def test_query(self):
        res = self.mysql_client.query("""desc a_account""")
        print res
        self.assertIsInstance(res, list, "this is a list")


if __name__ == '__main__':
    unittest.main()
