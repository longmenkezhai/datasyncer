# -*- coding: UTF-8 -*-

import unittest

import conf.config_handler as ch
from datasyncer.utils.postgre_util import PgClient as PC


class TestPgClint(unittest.TestCase):
    def setUp(self):
        self.pg_client = PC(database=ch.gp_db, user=ch.gp_user, password=ch.gp_pwd, host=ch.gp_host)

    def test_query(self):
        res = self.pg_client.query("""select * from a_account limit 10""")
        print res
        self.assertIsInstance(res, list, "this is a list")


if __name__ == '__main__':
    unittest.main()
