# -*- coding: UTF-8 -*-

import datasyncer.utils.datasyncer_util as du
from datasyncer.sync.sync_table_handler import SyncTableHandler as sth

# tables = ['a_account', 'a_activity_record', 'a_apply', 'a_blog', 'a_campaign', 'a_campaign_account', 'a_case',
#           'a_case_product', 'a_contact', 'a_contract', 'a_industry', 'a_invoice', 'a_lead', 'a_lead_convert_record',
#           'a_message', 'a_opportunity', 'a_opportunity_account', 'a_opportunity_contact', 'a_opportunity_process',
#           'a_opportunity_product', 'a_opportunity_stage_period', 'a_order', 'a_order_product', 'a_payment',
#           'a_payment_plan', 'a_product', 'a_schedule_member', 'a_solution', 'a_task_member', 'a_twitter_file',
#           'a_twitter_praise', 'a_twitter_question', 'a_visit_plan_time_logs', 'a_work_report', 'b_announcement',
#           'b_depart', 'b_entity_belong_type', 'b_entity_checkbox', 'b_group', 'b_personal_profile', 'b_select_item',
#           'b_tag', 'b_topic', 'b_user', 'p_custom_data']

# tables = ['p_custom_data_401', 'p_custom_data_401_ext', 'p_custom_data_402', 'p_custom_data_402_ext',
#           'p_custom_data_403', 'p_custom_data_403_ext', 'p_custom_data_404', 'p_custom_data_404_ext',
#           'p_custom_data_405', 'p_custom_data_405_ext', 'p_custom_data_406', 'p_custom_data_406_ext',
#           'p_custom_data_407', 'p_custom_data_407_ext', 'p_custom_data_408', 'p_custom_data_408_ext',
#           'p_custom_data_409', 'p_custom_data_409_ext', 'p_custom_data_410', 'p_custom_data_410_ext',
#           'p_custom_data_411', 'p_custom_data_411_ext', 'p_custom_data_412', 'p_custom_data_412_ext',
#           'p_custom_data_413', 'p_custom_data_413_ext', 'p_custom_data_414', 'p_custom_data_414_ext',
#           'p_custom_data_415', 'p_custom_data_415_ext', 'p_custom_data_416', 'p_custom_data_416_ext',
#           'p_custom_data_417', 'p_custom_data_417_ext', 'p_custom_data_418', 'p_custom_data_418_ext',
#           'p_custom_data_419', 'p_custom_data_419_ext', 'p_custom_data_420', 'p_custom_data_420_ext',
#           'p_custom_data_451', 'p_custom_data_451_ext', 'p_custom_data_452', 'p_custom_data_452_ext',
#           'p_custom_data_453', 'p_custom_data_453_ext', 'p_custom_data_454', 'p_custom_data_454_ext',
#           'p_custom_data_455', 'p_custom_data_455_ext', 'p_custom_data_456', 'p_custom_data_456_ext',
#           'p_custom_data_457', 'p_custom_data_457_ext', 'p_custom_data_458', 'p_custom_data_458_ext',
#           'p_custom_data_459', 'p_custom_data_459_ext', 'p_custom_data_460', 'p_custom_data_460_ext',
#           'p_custom_data_461', 'p_custom_data_461_ext', 'p_custom_data_462', 'p_custom_data_462_ext',
#           'p_custom_data_463', 'p_custom_data_463_ext', 'p_custom_data_464', 'p_custom_data_464_ext',
#           'p_custom_data_465', 'p_custom_data_465_ext', 'p_custom_data_466', 'p_custom_data_466_ext',
#           'p_custom_data_467', 'p_custom_data_467_ext', 'p_custom_data_468', 'p_custom_data_468_ext',
#           'p_custom_data_469', 'p_custom_data_469_ext', 'p_custom_data_470', 'p_custom_data_470_ext',
#           'p_custom_data_471', 'p_custom_data_471_ext', 'p_custom_data_472', 'p_custom_data_472_ext',
#           'p_custom_data_473', 'p_custom_data_473_ext', 'p_custom_data_474', 'p_custom_data_474_ext',
#           'p_custom_data_475', 'p_custom_data_475_ext', 'p_custom_data_476', 'p_custom_data_476_ext',
#           'p_custom_data_477', 'p_custom_data_477_ext', 'p_custom_data_478', 'p_custom_data_478_ext',
#           'p_custom_data_479', 'p_custom_data_479_ext', 'p_custom_data_480', 'p_custom_data_480_ext',
#           'p_custom_data_481', 'p_custom_data_481_ext', 'p_custom_data_482', 'p_custom_data_482_ext',
#           'p_custom_data_483', 'p_custom_data_483_ext', 'p_custom_data_484', 'p_custom_data_484_ext',
#           'p_custom_data_485', 'p_custom_data_485_ext', 'p_custom_data_486', 'p_custom_data_486_ext',
#           'p_custom_data_487', 'p_custom_data_487_ext', 'p_custom_data_488', 'p_custom_data_488_ext',
#           'p_custom_data_489', 'p_custom_data_489_ext', 'p_custom_data_490', 'p_custom_data_490_ext',
#           'p_custom_data_491', 'p_custom_data_491_ext', 'p_custom_data_492', 'p_custom_data_492_ext',
#           'p_custom_data_493', 'p_custom_data_493_ext', 'p_custom_data_494', 'p_custom_data_494_ext',
#           'p_custom_data_495', 'p_custom_data_495_ext', 'p_custom_data_496', 'p_custom_data_496_ext',
#           'p_custom_data_497', 'p_custom_data_497_ext', 'p_custom_data_498', 'p_custom_data_498_ext',
#           'p_custom_data_499', 'p_custom_data_499_ext', 'p_custom_data_500', 'p_custom_data_500_ext',
#           'p_custom_data_601', 'p_custom_data_601_ext']

# tables = ['p_custom_data_currency','p_custom_data_ext']
tables = ['a_goalpro_detail']

# xsy_behaviors.a_schedule_member
# xsy_behaviors.a_task_member
# xsy_behaviors.a_activity_record
# xsy_email.b_announcement
# xsy_feed.b_topic
# xsy_flow.a_apply
# xsy_flow.a_work_report
# xsy_group.b_group
# xsy_twitter.a_blog
# xsy_twitter.a_message
# xsy_twitter.a_twitter_file
# xsy_twitter.a_twitter_praise
# xsy_twitter.a_twitter_question

ss = sth()

for table in tables:
    ss.add_sync_table(source_db='isales_db_isales_001', source_table=table, target_db='test')

sync_table = du.get_table('bi_sync_table')
kafka_offset = du.get_table('bi_kafka_offset')

res = sync_table.select().execute()
for row in res:
    print row
