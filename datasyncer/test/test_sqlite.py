# -*- coding: UTF-8 -*-

from sqlalchemy.orm import *
from sqlalchemy import *

SQLITE_CONNECT_STRING = 'sqlite:///db/datasyncer.db'
engine = create_engine(SQLITE_CONNECT_STRING, echo=True)
DB_Session = sessionmaker(engine)
session = DB_Session()
meta = MetaData(engine)
sync_table = Table('sync_table', meta, autoload=True)
"""
CREATE TABLE sync_table
(
  id           INTEGER PRIMARY KEY NOT NULL,
  source_table VARCHAR(255)  UNIQUE   NOT NULL,
  target_table VARCHAR(255)  UNIQUE   NOT NULL,
  source_db    VARCHAR(255)        NOT NULL,
  target_db    VARCHAR(255)        NOT NULL,
  create_time  INTEGER,
  update_time  INTEGER,
  sub_table    INTEGER DEFAULT 0,
  sync         INTEGER DEFAULT 0
);
"""
# sync_table.insert().values(source_table='a_lead', target_table='a_lead', source_db='isales_db_isales_001',
#                            target_db='test',
#                            create_time=time.time(), update_time=time.time(), sub_table=0, sync=0).execute()
# sync_table.delete().where(sync_table.c.id == 1).execute()
# sync_table.update().where(sync_table.c.id == 1).values(sync=1).execute()
# result = sync_table.select().execute()
# sync_table.delete().where(sync_table.c.sync == 1).execute()
from datasyncer.sync.sync_table_handler import SyncTableHandler as sth

ss = sth()
ss.add_sync_table(source_table='a_case', target_db='test')
result = sync_table.select().execute()
for row in result:
    print row
