# -*- coding: UTF-8 -*-

import json

data = {u'xid': 18621081, u'database': u'isales_db_isales_001',
        u'data': {u'MOBILE_LOCATION_STATUS': None, u'COLLEAGUE_RELATION_DEPART': None,
                  u'PINYIN': u'j_yppvnuswyigulelwhy',
                  u'SPACE_ID': -1, u'DEPART_ID': 89493, u'CREATED_BY': 11491, u'TRY_LOCK_TIME': None,
                  u'TIMEZONE': u'Asia/Shanghai', u'LANGUAGE': 1, u'EMPLOYEE_CODE': None, u'LASTEST_LOGIN_AT': None,
                  u'PASSPORT': None, u'TRY_TIMES': None, u'PASSPORT_ID': 43796, u'STATUS': 0,
                  u'CREATED_AT': 1500963818274,
                  u'PASSWORD_RULE_ID': None, u'union_id': None, u'RESET_PASSWORD_FLG': None, u'SUPER_FLAG': 0,
                  u'ID': 65846, u'ICON': None, u'NAME': u'J_YPpVNuSWYIGuLELwHY', u'TENANT_ID': 27926,
                  u'UPDATED_AT': 1500963818274, u'FRESH_GUIDE_STATUS': 0, u'TYPE': 0}, u'ts': 1500963784,
        u'commit': True,
        u'table': u'b_user', u'type': u'delete'}
example = json.dumps({u'xid': 3820034, u'database': u'ingage', u'data': data})

from datasyncer.utils.parse_util import ParseHandler

ph = ParseHandler([example])

ph.parse_binlog()
