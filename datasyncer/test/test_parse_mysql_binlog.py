# -*- coding: UTF-8 -*-
import unittest

from ..utils.parse_util import ParseBinlog2Sql as PB


class TestParseMysqlBinlog(unittest.TestCase):
    def test_insert(self):
        binlog = {"database": "test", "table": "user", "type": "insert", "ts": 1494842561, "xid": 6155159,
                  "commit": True, "data": {"id": 2, "name": "ccc", "age": 223, "sex": "0"}}
        pb = PB(binlog)
        sql = pb.parse()
        self.assertIsNotNone(sql, sql)
        self.assertIsInstance(sql, str, 'real type is {0} not str'.format(type(sql)))
        self.assertEqual(sql, """INSERT INTO "user" (age,sex,id,name) VALUES (223,'0',2,'ccc')""")
        print sql

    def test_update(self):
        binlog = {"database": "test", "table": "user", "type": "update", "ts": 1494985723, "xid": 7798870,
                  "commit": True,
                  "data": {"id": 5, "name": "ass", "age": 223, "sex": "0"}, "old": {"name": "asa"}}
        pb = PB(binlog)
        sql = pb.parse()
        self.assertIsNotNone(sql, sql)
        self.assertIsInstance(sql, str, 'real type is {0} not str'.format(type(sql)))
        self.assertEqual(sql, """UPDATE "user" SET name='ass' WHERE age=223 and sex='0' and id=5 and name='asa'""")
        print sql

    def test_delete(self):
        binlog = {"database": "test", "table": "user", "type": "delete", "ts": 1494985954, "xid": 7801697,
                  "data": {"id": 5, "name": "ass", "age": 223, "sex": "0"}}
        pb = PB(binlog)
        sql = pb.parse()
        self.assertIsNotNone(sql, sql)
        self.assertIsInstance(sql, str, 'real type is {0} not str'.format(type(sql)))
        self.assertEqual(sql, """DELETE FROM "user" WHERE age=223 and sex='0' and id=5 and name='ass'""")
        print sql


if __name__ == '__main__':
    unittest.main()
