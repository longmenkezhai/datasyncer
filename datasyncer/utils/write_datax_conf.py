# -*- coding: UTF-8 -*-

import conf.config_handler as ch
import json


def get_conf(mysql_db, mysql_table, gp_db, gp_table):
    example = {
        "job": {
            "content": [
                {
                    "reader": {
                        "parameter": {
                            "password": ch.mysql_pwd,
                            "column": [
                                "*"
                            ],
                            "connection": [
                                {
                                    "jdbcUrl": [
                                        "jdbc:mysql://{mysql_host}:3306/{mysql_db}?zeroDateTimeBehavior=convertToNull&connectTimeout=5000".format(
                                            mysql_host=ch.mysql_host, mysql_db=mysql_db)],
                                    "table": [
                                        mysql_table
                                    ]
                                }
                            ],
                            "username": ch.mysql_user
                        },
                        "name": "mysqlreader"
                    },
                    "writer": {
                        "parameter": {
                            "password": ch.gp_pwd,
                            "column": [
                                "*"
                            ],
                            "connection": [
                                {
                                    "jdbcUrl": "jdbc:postgresql://{gp_host}:5432/{gp_db}".format(gp_host=ch.gp_host,
                                                                                                 gp_db=gp_db),
                                    "table": [
                                        gp_table
                                    ]
                                }
                            ],
                            "username": ch.gp_user
                        },
                        "name": "gpdbwriter"
                    }
                }
            ],
            "setting": {
                "speed": {"channel": 1}
            }
        }
    }

    return json.dumps(example, indent=4)


def write_file(path, conf):
    f = open(path, 'w')
    f.write(conf)
    f.close()
