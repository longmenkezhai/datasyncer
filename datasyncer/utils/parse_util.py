# -*- coding: UTF-8 -*-

import io
import os
import threading
import time

from postgre_util import PgSql
from conf import config_handler as ch

DML = ['insert', 'update', 'delete']
DDL = ['database-create', 'database-alter', 'database-drop', 'table-create', 'table-alter', 'table-drop']
DELETE_SQL = 'DELETE FROM {0} WHERE id in ({1})'


class ParseHandler:
    def __init__(self, binlogs, fmtparams=None):
        self.copy_pool = []
        self.delete_pool = {}
        self.table_pool = []
        self.items_pool = {}
        self.binlog_list = []
        self.fmtparams = fmtparams
        for binlog in binlogs:
            self.binlog_list.append(Binlog(binlog))

    def parse_binlog(self):
        self.__merge_binlogs()
        # print self.delete_pool
        map(lambda table: self.__write_copy_file(table), self.table_pool)

    def do_copy(self, pg_client, thread_num=3):
        if self.items_pool:
            tp = self.table_pool[:]
            while tp:
                thread_pool = []
                for i in range(thread_num):
                    if len(tp) > 0:
                        table = tp.pop()
                        if table not in self.items_pool:
                            continue
                        thread_pool.append(
                            threading.Thread(target=self.__do_single_copy, args=(table, pg_client)))
                for thread in thread_pool:
                    thread.start()
                while not self.check_threads(thread_pool):
                    time.sleep(1)

    def __do_single_copy(self, table, pg_client):
        # self.upload_csv('mdw', '{0}/data/{1}.csv'.format(ch.HOME_PATH, table), ch.COPY_PATH)
        cols = self.items_pool[table]
        csv_file = '{0}/data/{1}.csv'.format(ch.HOME_PATH, table)
        copy_sql = """COPY {0} ({1}) FROM '{2}' DELIMITER AS ',' csv QUOTE AS '"'""".format(table, ','.join(cols),
                                                                                            csv_file)
        print copy_sql
        # pg_client.copy_expert(_sql=copy_sql, _file=csv_file)
        pg_client.copy_from(_table=table, columns=cols, csv_file=csv_file, sep=',')

    def do_delete(self, pg_client, thread_num=3):
        if self.delete_pool:
            tp = self.table_pool[:]
            while tp:
                thread_pool = []
                for i in range(thread_num):
                    if len(tp) > 0:
                        thread_pool.append(
                            threading.Thread(target=self.__do_single_delete, args=(tp.pop(), pg_client)))
                for thread in thread_pool:
                    thread.start()
                while not self.check_threads(thread_pool):
                    time.sleep(1)

    def __do_single_delete(self, table, pg_client):
        delete_sql = DELETE_SQL.format(table, ','.join(self.delete_pool[table]))
        print delete_sql
        pg_client.execute(delete_sql)

    @staticmethod
    def check_threads(threads):
        finish = False
        if not threads:
            finish = True
        for thread in threads:
            if not thread.isAlive():
                finish = True
            else:
                finish = False
                break
        return finish

    @staticmethod
    def is_ddl(binlog):
        return True if binlog['type'] in DDL else False

    @staticmethod
    def execute_ddl_sql(binlog, pg_client):
        sql = binlog['sql']
        pg_client.execute(sql)

    def __merge_binlogs(self):
        tables = {}
        for binlog in self.binlog_list:
            table_name = binlog.table
            if table_name not in tables:
                tables[table_name] = [binlog]
            else:
                tables[table_name].append(binlog)
        for k, v in tables.items():
            merge_data = self.__merge(k, v)
            self.copy_pool.append({'table': k, 'datas': merge_data['copy_pool']})
            self.delete_pool[k] = merge_data['delete_pool']
            if k not in self.table_pool:
                self.table_pool.append(k)

    def __write_copy_file(self, table, path=None):
        table_datas = []
        if not path:
            path = '{0}/data/{1}.csv'.format(ch.HOME_PATH, table)
        for copy in self.copy_pool:
            if copy['table'] == table:
                table_datas = copy['datas']
        if not table_datas:
            return
        datas = map(lambda binlog: binlog.data, table_datas)
        items = sorted(datas[0].keys())
        self.items_pool[table] = items
        writer = self.__writer(open(path, 'w'), self.fmtparams)
        writer.writerows(map(lambda data: self.__sort_by_key(data, items), datas))

    @staticmethod
    def upload_csv(host, src_path, target_path, identity_file=None):
        identity_opt = '' if not identity_file else '-i {0}'.format(identity_file)
        cmd = 'scp {0} {1} {2}:{3}'.format(identity_opt, src_path, host, target_path)
        os.popen(cmd)

    @staticmethod
    def upload_pipe(path):
        fd = os.open(path, os.O_RDWR | os.O_CREAT)
        return os.fdopen(fd)

    @staticmethod
    def __sort_by_key(_map, items=None):
        if not items:
            items = sorted(_map.keys())
        data_list = []
        for i in items:
            data_list.append(_map[i])
        return data_list

    @staticmethod
    def __merge(table, binlogs):
        delete_pool = []
        copy_pool = []
        binlogs_ids = {}
        for log in binlogs:
            if log.id not in binlogs_ids:
                binlogs_ids[log.id] = [log]
            else:
                binlogs_ids[log.id].append(log)
        map(lambda _id: delete_pool.append(str(_id)), binlogs_ids.keys())
        for k, v in binlogs_ids.items():
            last_binlog = v[-1]
            if last_binlog.type == DML[0]:
                # print 'insert'
                copy_pool.append(last_binlog)
            elif last_binlog.type == DML[1]:
                # print 'update'
                copy_pool.append(last_binlog)
            elif last_binlog.type == DML[2]:
                # print 'delete'
                pass
            else:
                raise Exception('wrong opts in pool')
        return {'delete_pool': delete_pool, 'copy_pool': copy_pool}

    @staticmethod
    def __writer(csvfile, fmtparams=None):
        if not fmtparams:
            fmtparams = {}
        delimiter = fmtparams.get('delimiter', PGCsvWriter.DEFAULT_DELIMITER)
        escapechar = fmtparams.get('escapechar', PGCsvWriter.DEFAULT_ESCAPE)
        lineterminator = fmtparams.get('lineterminator', PGCsvWriter.DEFAULT_LINE_TERMINATOR)
        quotechar = fmtparams.get('quotechar', PGCsvWriter.DEFAULT_QUOTE)
        null = fmtparams.get('null', PGCsvWriter.DEFAULT_NULL)
        force_quote = fmtparams.get('force_quote', PGCsvWriter.DEFAULT_FORCE_QUOTE)
        return PGCsvWriter(csvfile, delimiter, escapechar, lineterminator,
                           quotechar, null, force_quote)


class PGCsvWriter(object):
    DEFAULT_DELIMITER = ','
    DEFAULT_NULL = ''
    DEFAULT_QUOTE = '"'
    DEFAULT_ESCAPE = DEFAULT_QUOTE
    DEFAULT_FORCE_QUOTE = False
    DEFAULT_LINE_TERMINATOR = '\n'

    def __init__(self, csvfile, delimiter, escapechar, lineterminator,
                 quotechar, null, force_quote):
        self._csvfile = csvfile
        self._delimiter = delimiter
        self._escapechar = escapechar
        self._lineterminator = lineterminator
        self._quotechar = quotechar
        self._null = null
        self._force_quote = force_quote

    def _escape_string(self, s):
        need_quote = self._force_quote
        out = io.StringIO()

        for ch in s:
            if ch == self._quotechar or ch == self._escapechar:
                need_quote = True
                out.write(unicode(self._escapechar))
            elif ch == self._delimiter or ch == '\r' or ch == '\n' or ch == self._null:
                need_quote = True

            out.write(ch)

        if need_quote or s == self._null:
            return "%s%s%s" % (self._quotechar, out.getvalue(), self._quotechar)
        else:
            return out.getvalue()

    def writerow(self, row):
        for i in range(len(row)):
            col = row[i]

            if col is None:
                s = self._null
            elif isinstance(col, str):
                s = col.decode('utf-8')
                s = self._escape_string(s)
            elif isinstance(col, unicode):
                s = self._escape_string(col)
            else:
                s = unicode(col)

            self._csvfile.write(self.get_bytes(s))

            if i < len(row) - 1:
                self._csvfile.write(self.get_bytes(self._delimiter))

        self._csvfile.write(self.get_bytes(self._lineterminator))

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

    @staticmethod
    def get_bytes(s):
        if s is None:
            return None
        elif isinstance(s, unicode):
            return s.encode('utf8')

        return s


class Binlog(object):
    def __init__(self, binlog):
        self.__dict__.update(binlog)
        if 'ID' in self.data:
            self.id = self.data['ID']
        elif 'id' in self.data:
            self.id = self.data['id']
        else:
            self.id = ""


class ParseBinlog2Sql:
    DML = ['insert', 'update', 'delete']
    INSERT_SQL = 'INSERT INTO {0} ({1}) VALUES ({2})'
    UPDATA_SQL = 'UPDATE {0} SET {1} WHERE {2}'
    DELETE_SQL = 'DELETE FROM {0} WHERE {1}'

    def __init__(self, datas):
        self.datas = datas
        self.database = datas['database']
        self.table = PgSql.identifier(datas['table'])
        self.type = datas['type']
        # self.commit = datas['commit']
        self.data = datas['data']

    def parse(self):
        if self.type in self.DML:
            if self.type == self.DML[0]:
                # insert
                cols = ','.join(map(lambda e: e.lower(), self.data.keys()))
                values = ','.join(PgSql.safe_entry(self.data.values()))
                sql = self.INSERT_SQL.format(self.table, cols, values)
                return sql
            elif self.type == self.DML[1]:
                # update
                old = self.datas['old']
                u = []
                w = []
                for nk in self.data.keys():
                    for ok in old.keys():
                        if ok == nk:
                            u.append("{0}={1}".format(nk, PgSql.safe(self.data[nk])))
                            w.append("{0}={1}".format(ok, PgSql.safe(old[ok])))
                            break
                        else:
                            w.append("{0}={1}".format(nk, PgSql.safe(self.data[nk])))
                sql = self.UPDATA_SQL.format(self.table, ','.join(u), ' and '.join(w))
                return sql
            elif self.type == self.DML[2]:
                # delete
                w = []
                for k, v in self.data.items():
                    w.append("{0}={1}".format(k, PgSql.safe(v)))
                sql = self.DELETE_SQL.format(self.table, ' and '.join(w))
                return sql
