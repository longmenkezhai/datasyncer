# -*- coding: UTF-8 -*-

from datasyncer.utils.db_client import Client


class PgClient(Client):
    def __init__(self, database, user, password, host, db='postgres'):
        super(PgClient, self).__init__(database, user, password, host, db)

    def copy_from(self, _table, columns, csv_file, sep):
        connection = self.source_engine.raw_connection()
        cursor = connection.cursor()
        try:
            cursor.copy_from(file=open(csv_file), table=_table, columns=columns, sep=sep, null="")
            connection.commit()
        finally:
            cursor.close()

    def copy_expert(self, _sql, _file, size=8192):
        connection = self.source_engine.raw_connection()
        cursor = connection.cursor()
        try:
            cursor.copy_expert(_sql, open(_file), size=size)
            connection.commit()
        finally:
            cursor.close()


class PgSql(object):
    @staticmethod
    def safe_entry(entries):
        new_entries = []
        for e in entries:
            if type(e) == unicode or type(e) == str:
                new_entries.append("'" + e + "'")
            elif e == 0:
                new_entries.append(str(e))
            elif not e:
                new_entries.append('null')
            else:
                new_entries.append(str(e))
        return new_entries

    @staticmethod
    def safe(entry):
        if type(entry) == unicode or type(entry) == str:
            return "'" + entry + "'"
        else:
            return str(entry)

    @staticmethod
    def identifier(entry):
        return '"{0}"'.format(entry) if entry else '""'
