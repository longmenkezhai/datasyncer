# -*- coding: UTF-8 -*-

import json

from kafka import KafkaConsumer
from kafka import KafkaProducer
from kafka import TopicPartition

from conf.config_handler import kafka_hosts
from conf.config_handler import kafka_topic


class KafkaUtil:
    DEFAULT_TOPIC = kafka_topic
    DEFAULT_PARTITION = 0
    DEFAULT_TOPIC_PARTITION = TopicPartition(DEFAULT_TOPIC, DEFAULT_PARTITION)

    def __init__(self, server_address=kafka_hosts, topic=DEFAULT_TOPIC):
        self.server_address = server_address
        self.topic = topic

    def get_producer(self, **configs):
        return KafkaProducer(bootstrap_servers=self.server_address, **configs)

    def get_consumer(self, **configs):
        return KafkaConsumer(self.topic, bootstrap_servers=self.server_address,
                             value_deserializer=lambda m: json.loads(m), **configs)
        # return KafkaConsumer(self.topic, bootstrap_servers=self.server_address, **configs)
