# -*- coding: UTF-8 -*-
import traceback
from sqlalchemy.orm import *
from sqlalchemy import *
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.dialects.mysql import DOUBLE, TINYINT, LONGTEXT, MEDIUMTEXT
import conf.config_handler as ch
from sqlalchemy import event


@compiles(DOUBLE, 'postgresql')
def compile_double_postgres_mysql(element, compiler, **kw):
    return compiler.visit_DOUBLE_PRECISION(element, **kw)


@compiles(TINYINT, 'postgresql')
def compile_tinyint_postgres_mysql(element, compiler, **kw):
    return compiler.visit_SMALLINT(element, **kw)


@compiles(LONGTEXT, 'postgresql')
def compile_longtext_postgres_mysql(element, compiler, **kw):
    return compiler.visit_TEXT(element, **kw)


@compiles(MEDIUMTEXT, 'postgresql')
def compile_mediumtext_postgres_mysql(element, compiler, **kw):
    return compiler.visit_TEXT(element, **kw)

PG_CONNECT_STRING = 'postgres://{0}:{1}@{2}/{3}'.format(ch.gp_user, ch.gp_pwd, ch.gp_host,
                                                        ch.gp_db)
target_engine = create_engine(PG_CONNECT_STRING, echo=True)
target_meta = MetaData(target_engine)


# def sync_db():
#     """
#     synchronize all table
#     :return:
#     """
#     DB_Session = sessionmaker(source_engine)
#     session = DB_Session()
#     tables = session.execute('show tables').fetchall()
#     for table in tables:
#         try:
#             t = table[0]
#             source_table = Table(t, source_meta, autoload=True)
#             target_table = source_table.tometadata(target_meta, schema=None)
#             target_table.create(target_engine, checkfirst=True)
#         except SnycTableRunTimeException as e:
#             msg = traceback.format_exc()
#             print msg
#     session.close()


def sync_table(source_db, source_table):
    def listen_for_reflect(inspector, table, column_info):
        column_info['name'] = column_info['name'].lower()

    try:
        MYSQL_CONNECT_STRING = 'mysql://{0}:{1}@{2}/{3}?charset=utf8'.format(ch.mysql_user, ch.mysql_pwd, ch.mysql_host,
                                                                             source_db)
        source_engine = create_engine(MYSQL_CONNECT_STRING, echo=True)
        source_meta = MetaData(source_engine)
        source_table = Table(source_table, source_meta, autoload=True, listeners=[
            ('column_reflect', listen_for_reflect)
        ])
        target_table = source_table.tometadata(target_meta, schema=None)
        target_table.indexes.clear()
        target_table.create(target_engine, checkfirst=True)
    except SnycTableRunTimeException:
        msg = traceback.format_exc()
        print msg


def drop_table(target_table):
    try:
        target_table = Table(target_table, target_meta, autoload=True)
        target_table.drop(target_engine, checkfirst=True)
    except SnycTableRunTimeException:
        msg = traceback.format_exc()
        print msg


class SnycTableRunTimeException(RuntimeError):
    def __init__(self, error_msg='some index can not be synchronized,please ignore this'):
        RuntimeError.__init__(self, error_msg=error_msg)

