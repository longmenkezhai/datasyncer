# -*- coding: UTF-8 -*-

from sqlalchemy.orm import *
from sqlalchemy import *
import conf.config_handler as ch

# SQLITE_CONNECT_STRING = 'sqlite:///db/datasyncer.db'
MYSQL_CONNECT_STRING = 'mysql://{0}:{1}@{2}/{3}?charset=utf8'.format(ch.datasync_user, ch.datasync_pwd,
                                                                     ch.datasync_host,
                                                                     ch.datasync_db)
engine = create_engine(MYSQL_CONNECT_STRING, echo=True)
DB_Session = sessionmaker(engine)
session = DB_Session(autocommit=True)
meta = MetaData(engine)


def get_table(table_name):
    _table = Table(table_name, meta, autoload=True)
    return _table
