# -*- coding: UTF-8 -*-
import os
import conf.config_handler as ch
from flask_restful import reqparse, abort, Api, Resource, request
from datasyncer.utils.postgre_util import PgClient as PC

parser = reqparse.RequestParser()


class BiMonitor(Resource):
    def post(self):
        args = parser.parse_args()
        if request.endpoint is 'dropTable':
            db, target_table = args.get('target_table'), args.get('db')
            if not db or not target_table:
                return 'db and target is missing , please check it'
            self.drop_table_action(db, target_table)
        elif request.endpoint is 'doSync':
            self.do_sync_action()
        return 201

    @staticmethod
    def drop_table_action(db, target_table):
        pg_client = PC(database=db, user=ch.gp_user, password=ch.gp_pwd, host=ch.gp_host)
        pg_client.drop_table(target_table)

    @staticmethod
    def do_sync_action():
        os.system(ch.HOME_PATH + '/bin/start.sh')
