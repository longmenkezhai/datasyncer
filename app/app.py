# -*- coding: UTF-8 -*-
from flask import Flask
from flask_restful import Resource, Api
from bi_monitor import BiMonitor

app = Flask(__name__)
api = Api(app)

api.add_resource(BiMonitor, '/dropTable', endpoint='dropTable')
api.add_resource(BiMonitor, '/doSync', endpoint='doSync')

if __name__ == '__main__':
    app.run(host='192.168.0.191')
