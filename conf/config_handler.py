# -*- coding: UTF-8 -*-

import ConfigParser
import logging.config

# reload(sys)
# sys.setdefaultencoding("utf-8")

cf = ConfigParser.ConfigParser()
cf.read("conf/datasync.conf")

# datasync
HOME_PATH = cf.get('datasync', 'home_path')
max_commit = int(cf.get('datasync', 'max_commit'))
commit_interval = int(cf.get('datasync', 'commit_interval'))
offset = int(cf.get('datasync', 'offset'))
wait_time = int(cf.get('datasync', 'wait_time'))


# logger
def get_logger():
    logging.basicConfig(filename='%s/log/datasync.log' % HOME_PATH, level=logging)
    logger = logging.getLogger("datasync")
    return logger


# kafka
kafka_hosts_str = cf.get('kafka', 'host')
kafka_hosts = cf.get('kafka', 'host').split(',')
kafka_topic = cf.get('kafka', 'topic')

# mysql
mysql_host = cf.get('mysql', 'host')
mysql_user = cf.get('mysql', 'user')
mysql_pwd = cf.get('mysql', 'pwd')
mysql_db = cf.get('mysql', 'db')

# GP
gp_host = cf.get('gp', 'host')
gp_user = cf.get('gp', 'user')
gp_pwd = cf.get('gp', 'pwd')
gp_db = cf.get('gp', 'db')

# maxwell
mw_host = cf.get('maxwell', 'host')
mw_user = cf.get('maxwell', 'user')
mw_pwd = cf.get('maxwell', 'pwd')
mw_db = cf.get('maxwell', 'db')
rp_host = cf.get('maxwell', 'rp_host')
rp_user = cf.get('maxwell', 'rp_user')
rp_pwd = cf.get('maxwell', 'rp_pwd')
ddl = cf.get('maxwell', 'ddl')

# datasync_db
datasync_host = cf.get('datasync_db', 'host')
datasync_user = cf.get('datasync_db', 'user')
datasync_pwd = cf.get('datasync_db', 'pwd')
datasync_db = cf.get('datasync_db', 'db')
