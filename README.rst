DataSyncer 使用说明
=========================
一、安装环境
-------------------------
执行 script/env_setup.sh 文件，自动安装环境

二、参数配置
-------------------------

1. 要同步的mysql连接信息
:::::::::::::::::::::::::
- [mysql]
- host = 192.168.0.75
- db = isales_db_isales_001
- user = ingage
- pwd = ingage

2. 要同步到的greenplum连接信息
:::::::::::::::::::::::::
- [gp]
- host = mdw
- db = test2
- user = gpadmin
- pwd = gpadmin

3. maxwell 相关参数
:::::::::::::::::::::::::
- [maxwell]
- # maxwell 数据库连接信息
- host = nmaster
- db = maxwell
- user = root
- pwd = root
- # 要同步的mysql连接信息
- # 需要的用户权限： SELECT, REPLICATION CLIENT, REPLICATION SLAVE
- rp_host = 192.168.0.75
- rp_user = repl
- rp_pwd = 3cqscbrOnly1
- # 是否需要开启ddl 同步
- ddl = false

4. kafka 连接信息，集群用 "," 分开
:::::::::::::::::::::::::
- [kafka]
- host = 192.168.0.191:9092
- topic = m7

5. datasync各项参数
:::::::::::::::::::::::::
- [datasync]
- # 本项目的homepath路径，非常重要必填
- home_path = /home/git/datasync
- # 最大提交条数
- max_commit = 10000
- # 提交最大等待时间
- commit_interval = 5
- # 队列满时的等待时间
- wait_time = 1
- #  LATEST = -1 EARLIEST = -2
- # kafka的offset起始执行点，-1 是最后一次，-2 是从头开始
- offset = -2

6. datasync数据库的连接信息
:::::::::::::::::::::::::
- [datasync_db]
- host = 192.168.0.197
- db = xsy_bi
- user = xsy_bi
- pwd = xsy_bi

三、启动service
-------------------------
执行 bin/start.sh 文件，没有报错即启动成功，日志生成到 log 目录下